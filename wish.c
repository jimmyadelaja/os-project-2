//oluwatobi adelaja

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>


//Global Variables
char *argumentA;
int argumentB;
char *argumentC;

//---threaded Processes---
void * parProcesses() {
	char *arrayCmds = argumentA;
	int commandType = argumentB;
	//when cd command is called
	if (commandType == 1) {
		char*withoutNewLine[100];
		withoutNewLine[0] = strtok(arrayCmds[1], "\n");

		char fileLocat[100];
		strcpy(fileLocat, withoutNewLine[0]);
		printf("In cd thread if-stat");

		int chDir = chdir(fileLocat);
		//printf("array of commands: %s", arrayCmds[1]);
		printf("%d\n", chDir);
		if (chDir == 0) {
			printf("Directory has been changed\n");
		}
		else if (chDir != 0) {
			printf("File Directory does not exist\n");
		}
		printf("Done with cd Thread");
	}

	//redirection
	if (commandType == 2) {
		//remove new line character
		char Psearch_string[1024];
		strcpy(Psearch_string, arrayCmds[4]);
		char *Parray[50];
		int loop;

		Parray[0] = strtok(Psearch_string, "\n");
		if (Parray[0] == NULL) {
			printf("No test to search.\n");
			exit(0);
		}

		for (loop = 1; loop < 50; loop++) {
			Parray[loop] = strtok(NULL, " ");
			if (Parray[loop] == NULL)
				break;
		}

		redirProcess(arrayCmds[0], Parray[0]);
	}

	//processes
	if (commandType == 3) {
		char *fileLocation = argumentC;

		//remove newLine Character
		char*withoutNewLine[100];
		withoutNewLine[0] = strtok(arrayCmds[0], "\n");

		char* fileName = withoutNewLine[0];
		char fileLocat[1024];
		strcpy(fileLocat, fileLocation);
		strcat(fileLocat, fileName);

		//execute executable file
		int accessGrant = access(fileLocat, X_OK);
		//				int accessGrant2 = access(usrFilePath, X_OK);  //checking access on /usr/bin/;
		if (accessGrant == 0) {
			process(fileLocat);

			//check redirection

		}
		//				if (accessGrant2 == 0){
		//					process(usrFilePath);
		//				}
		else {
			printf("File is not executable\n");
		}
	}
}


int main(int argc, char* argv[]){
	
	//Global Variables
	char *fileLocation = "/bin/";
	char usrFileLocation[10] = "/usr";
	char *usrFilePath;
	
	
	
	//INTERACTIVE Mode
    if (argc == 1){
		int endLoop = 1;
		while (endLoop == 1){
			
			//Read Input
			char *buffer;
    		size_t bufsize = 32;
    		size_t characters;

    		buffer = (char *)malloc(bufsize * sizeof(char));
    		if( buffer == NULL){
        		perror("Unable to allocate buffer");
        		exit(1);
    		}
			int cond = 0;
    		printf("wish> ");
    		characters = getline(&buffer,&bufsize,stdin); 
			//printf(&buffer[0]);

			//parallel commands
			if (strstr(&buffer[0], "&") != NULL) {
				//tokenizaton to seperate the ampersand
				char Psearch_string[1024];
				strcpy(Psearch_string,&buffer[0]);
				char *cmd_array[50];
				int loop;
				
				cmd_array[0]=strtok(Psearch_string,"&");
				if(cmd_array[0]==NULL){
					printf("No test to search.\n");
					exit(0);
				}
				
				for(loop=1;loop<50;loop++){
					cmd_array[loop]=strtok(NULL,"&");
					if(cmd_array[loop]==NULL)
						break;
				}

				int y = 0;
				//seperating and running instructions
				while (cmd_array[y] != NULL){
					cond = 0;
					printf(cmd_array[y]);

					//seperating commadn words by a space
					char search_string[1024];
					strcpy(search_string,cmd_array[y]);
					char *array[50];
					int loop;
					
					array[0]=strtok(search_string," ");
					if(array[0]==NULL){
						printf("No test to search.\n");
						exit(0);
					}
					
					for(loop=1;loop<50;loop++){
						array[loop]=strtok(NULL," ");
						if(array[loop]==NULL)
							break;
					}

					//when cd command is called
					if ((strcmp("cd", array[0])) == 0){
						//perform cd command
						//printf(&buffer[3]);
						
						char*withoutNewLine[100];
						withoutNewLine[0] = strtok(array[1], "\n");
						
						char fileLocat[100];
						strcpy(fileLocat, withoutNewLine[0]);
						
						cond = 1;
						
						int chDir = chdir(fileLocat);
						printf("%d\n",chDir);
						if (chDir == 0){
							//printf("Directory has been changed\n");
						}
						else if (chDir != 0){
							//printf("File Directory does not exist\n");
						}
					}

					//executing files & processes
					if (cond == 0){
						//remove newLine Character
						char*withoutNewLine[100];
						withoutNewLine[0] = strtok(array[0], "\n");
						
						char* fileName = withoutNewLine[0];
						char fileLocat [1024];
						strcpy(fileLocat,fileLocation);
						strcat(fileLocat, fileName);
						printf(fileLocat);
						system(fileLocat);
						
						//execute executable file
						int accessGrant = access(fileLocat, X_OK);
						if (accessGrant == 0){
							//process(fileLocat);

							//check redirection

						}
						if (cond == -1){
							printf("File is not executable\n");
						}
					}

					//Done
					y++;
				}
			}
			
			//Handling Commands & Seperating Arguements by Tokenization
			char search_string[1024];
			strcpy(search_string,&buffer[0]);
			char *array[50];
			int loop;
			
			array[0]=strtok(search_string," ");
			if(array[0]==NULL){
				printf("No test to search.\n");
				exit(0);
			}
			
			for(loop=1;loop<50;loop++){
				array[loop]=strtok(NULL," ");
				if(array[loop]==NULL)
			  		break;
			}
			

			//Check for the different commands
		
			//exit loop and end program
			if (strcmp("exit\n", array[0]) == 0){
				break;
			}

			
			//when cd command is called
			if ((strcmp("cd", array[0])) == 0){
				//perform cd command
				//printf(&buffer[3]);
				
				char*withoutNewLine[100];
				withoutNewLine[0] = strtok(&buffer[3], "\n");
				
				char fileLocat[100];
				strcpy(fileLocat, withoutNewLine[0]);
				
				cond = 1;
				
				int chDir = chdir(fileLocat);
				printf("%d\n",chDir);
				if (chDir == 0){
					//printf("Directory has been changed\n");
				}
				else if (chDir != 0){
					//printf("File Directory does not exist\n");
				}
			}

			//Redirection
			if (cond == 4) {
				if (strstr(&buffer[0], "-la") != NULL) {
					//remove new line character
					char Psearch_string[1024];
					strcpy(Psearch_string, array[4]);
					char *Parray[50];
					int loop;

					Parray[0] = strtok(Psearch_string, "\n");
					if (Parray[0] == NULL) {
						printf("No test to search.\n");
						exit(0);
					}

					for (loop = 1; loop < 50; loop++) {
						Parray[loop] = strtok(NULL, " ");
						if (Parray[loop] == NULL)
							break;
					}

					redirProcess(array[0], Parray[0]);
				}
			}


			//executing files & processes
			else if (cond == 0){
				//remove newLine Character
				char*withoutNewLine[100];
				withoutNewLine[0] = strtok(&buffer[0], "\n");
				
				char* fileName = withoutNewLine[0];
				char fileLocat [1024];
				strcpy(fileLocat,fileLocation);
				strcat(fileLocat, fileName);
				system(fileLocat);
				
				//execute executable file
				int accessGrant = access(fileLocat, X_OK);
				if (accessGrant == 0){
					//process(fileLocat);

					//check redirection

				}
				if (cond == -1){
					printf("File is not executable\n");
				}
			}
		}
	}
	
	
	
	
	
	
	
	//BATCH MODE --- abandoned
	if (argc == 2){	
		//Variables
		FILE *infile = fopen(argv[1], "r");
		char * line = NULL;
	    size_t len = 0;
	    size_t read;
		int counter = 1;
		
        //checks if file cannot be opened
		if (infile == NULL){
			printf("wish: cannot open file: ");
			exit(1);
		}
        //opens file and prints it out
		else{
			char line[1024] ;
			while (fgets(line , sizeof(line) , infile )!= NULL){
				if (strstr(line , argv[1] )!= NULL){
				printf("%s",line);
				}
			}
		}
        //close file
		fclose(infile);
	}
	exit(0);
    
}





//<--------FUNCTIONS---------->
//creating a process to execute a file
void process(char *fileName){
	
	pid_t rc, rc_wait;
	int status;
	rc = fork();
	//printf(fileName);
	char *args[102];
	args[0] = fileName;
	args[1] = NULL; 
	//args[1] = NULL;
	if (rc < 0) { // fork failed; exit
		fprintf(stderr, "fork failed\n");
		exit(1);
	}
	else if (rc == 0) { // child (new process)
		if (execvp(args[0], args) == -1){
			perror("wish");
		}
		exit(EXIT_FAILURE);
	}
	else { // parent goes down this path (main)
		do{
			rc_wait = waitpid(rc, &status, WUNTRACED);
		}while(!WIFEXITED(status) && !WIFSIGNALED(status));
	}
}



//remove subtring from string
void removeChar(char *str, char garbage) {

	char *src, *dst;
	for (src = dst = str; *src != '\0'; src++) {
		*dst = *src;
		if (*dst != garbage) dst++;
	}
	*dst = '\0';
}


//redirecting process function
void redirProcess(char * executable, char * fileName) {
	int filefd = open(fileName, O_WRONLY | O_CREAT, 0666);
	if (!fork()) {
		close(1);//Close stdout
		dup(filefd);
		execlp(executable, executable, NULL);
	}
	else {
		close(filefd);
		wait(NULL);
	}
}
